# NOTE #

Its a raw commit bin, there are no unit-tests written yet for this code nor has the code been extensively tested.
Strongly discouraged to put this, or any files in this repository in production.

### Unit test coverage ###
0% - :( 

Have to investigate compatibility with a PHP unit-test framework for hack language.

### hh_client compatibility ###

98% - pass strongtype checker. (//strict mode.)

### C++ plugins ###

Special revised Ubuntu compiled C++ plugins for PGSQL and MSGPACK for HHVM are included.

## Milestone 2 ##

- Environment object (to allow for development/staging/production)
- ErrorHandling
- Unit tests