<?hh // strict

namespace Hacktacular\User\Authentication\oAuth;

use Hacktacular\Core\Routing\Request;

class Response
{
	public function __construct(): void
	{
		
	}

	/**
	 * Checks whether or not the supplied response is valid
	 */
	private function validateResponse(): bool
	{
		// Shorthand notation
		$query = Request::$query;

		// We have received an error so invalid response.
		if ($query->contains('error')) {
			return false;
		}

		// Retrieve google's reply state
		$state = Request::$query->get('state');

		return false;
	}
}