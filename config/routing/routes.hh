<?hh //strict

namespace bootstrap;

use Hacktacular\Core\Routing\Router;

function addRoutes(): void
{
	/**
	 * User routes can be created below
	 * For more information on how to create routes, please refer to our docs.
	 */
	Router::addRoute('/user',          'Hacktacular\User\Controller\UserController::info');
	Router::addRoute('/user/login',    'Hacktacular\User\Controller\UserController::login');
	Router::addRoute('/user/logout',   'Hacktacular\User\Controller\UserController::logout');
	Router::addRoute('/user/register', 'Hacktacular\User\Controller\UserController::register');

	Router::addRoute('/user/profile',  'Hacktacular\User\Controller\ProfileController::getProfile');

	Router::addRoute('/content/{cid}', 'Hacktacular\IO\API\Handler\ContentHandler::fetch');
	// Disallow access to root
	Router::addRoute('/', function() {	});

	/**
	 * End of route addition section.
	 */
}
