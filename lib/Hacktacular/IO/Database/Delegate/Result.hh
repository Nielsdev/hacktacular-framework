<?hh // strict

namespace Hacktacular\IO\Database\Delegate;

use Hacktacular\IO\Database\Query;
class Result implements \JsonSerializable
{
	/**
	 * Result map
	 * @var map
	 */
	private Map<string, mixed> $data = Map {};

	/**
	 * Change tracking boolean
	 * @var bool
	 */
	private bool $changed = false;

	/**
	 * Table to save changes in
	 * @var string
	 */
	private string $relation = '';

	/**
	 * Retrieves requested column
	 * @param string $column The column
	 * @return mixed The value
	 */
	public function __get(string $column): mixed
	{
		return $this->data->get($column);
	}

	/**
	 * Populates the supplied column with supplied value
	 * @param string $column The column
	 * @param string $value The value
	 */
	public function __set(string $column, mixed $value): void
	{
		$previous = $this->data->get($column);

		if ($previous !== $value) {
			$this->data->set($column, $value);
			$this->changed = true;
		}
	}

	/**
	 * Sets the table this Result is from
	 * @param string $table
	 */
	public function setRelation(string $relation): void
	{
		$this->relation = $relation;
	}

	/**
	 * Serializes the Result to JSON
	 * @return Map
	 */
	public function jsonSerialize(): Map
	{
		return $this->data;
	}

	/**
	 * Saves the current Delegate Result
	 */
	public function save(): void
	{
		Query::save($this->data, $this->relation);
	}

	public function delete(): void
	{

	}
}