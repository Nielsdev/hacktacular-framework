<?hh // strict

namespace Hacktacular\Core\Security;

use Hacktacular\Core\Routing\Request;
use Hacktacular\Core\Cache\CacheController;

abstract class IdentityFactory
{
	const string TOKEN_HEADER = 'X_SESSION_TOKEN';

	/**
	 * Starts an identiy, or loads one if a token is supplied
	 */
	public static function startInstance(): void
	{
		// Shorthand notation
		$headers = Request::$headers;

		if ($headers->contains(self::TOKEN_HEADER)) {
			$token = (string)$headers->get(self::TOKEN_HEADER);
			$identity = CacheController::retrieve('identity', array($token));

			// Attempt to boot up the identity
			if ($identity instanceof Identity) {
				// If regeneration is successful then we're done here.
				if ($identity->regenerateSession()) {
					return;
				}
			}
		}

		// Start session
		session_start();

		// Attempt to retrieve instance of this session
		$identity = self::getInstance();

		// If we have an identity for our current session, we're done as well
		if ($identity instanceof Identity) {
			return;
		}

		// Still no identity. so at this point, we will create a new one
		$identity = new Identity();
		$identity->addSessionId(session_id());
		$identity->save();
	}

	/**
	 * Removes the (current)identity
	 */
	public static function removeInstance(): void
	{
		$identity = self::getInstance();

		if ($identity === null) {
			// no current identity, let's just return
			return;
		}

		// Removes the identity
		$identity->delete();
	}

	/**
	 * Retrieves current identity
	 * @return Identity
	 */
	public static function getInstance(): ?Identity
	{
		// Attempt to retrieve identity
		$identity = CacheController::retrieve('identity', array(session_id()));

		if (!($identity instanceof Identity)) {
			$identity = null;
		}

		return $identity;
	}
}