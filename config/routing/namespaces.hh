<?hh // strict

namespace bootstrap;

function addNamespaces(\Psr4AutoloaderClass $loader): void
{
	// Bierjury namespace
	$loader->addNamespace('Bierjury', ROOT_DIR . '/lib/Bierjury');
}