<?hh // partial
/**
 * Needs to be partial, to be compatible with PHP's SessionHandlerInterface!
 */

namespace Hacktacular\Core\Security;

use Hacktacular\Core\Setting\SettingSource;

class MemcacheSessionHandler implements \SessionHandlerInterface
{
	/**
	 * The Identity token, used for many services
	 */
	private string $token = '';

	/**
	 * Memcached instance
	 */
	private \Memcached $memcache;

	/**
	 * Constructor
	 */
	public function __construct(): void
	{
		// Prepare for take-off
		$this->initialize();
	}

	/**
	 * Initializes requirements for session handling
	 */
	private function initialize(): void
	{
		// Get memcached
		$this->memcache = new \Memcached();

		// Retrieve memcached settings
		$host = SettingSource::getSetting('host', 'memcache');
		$port = SettingSource::getSetting('port', 'memcache');

		$this->memcache->addServer($host, $port);

		// Register
		// UNSAFE
		session_set_save_handler($this, true);
	}

	/**
	 * Starts an identity
	 * @param string $save_path Neglected, but has to be implemented
	 * @param string $name Name of the session
	 */
	public function open($save_path, $name): bool
	{
		return true;
	}

	public function close(): bool
	{
		return true;
	}

	public function gc($maxlifetime): bool
	{
		return true;
	}

	public function write($id, $data): mixed
	{
		$res = $this->memcache->set($id, $data, 1500);

		return $res;
	}

	public function read($sessionid): mixed
	{
		$data = $this->memcache->get($sessionid);

		return $data;
	}

	/**
	 * Destroys an identity
	 * @param string $sessionid
	 */
	public function destroy($session_id): bool
	{
		$this->memcache->delete($session_id);

		return true;
	}
}