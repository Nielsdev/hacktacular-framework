<?hh // strict

namespace Hacktacular\User\Controller;

use Hacktacular\Core\Routing\Request;
use Hacktacular\IO\API\Response\ResponseFactory;
use Hacktacular\IO\API\Response\BaseResponse;
use Hacktacular\User\Authentication\Login;
use Hacktacular\IO\Database\Query;
use Hacktacular\Core\Security\IdentityFactory;
use Hacktacular\Core\Security\Encryption;
use Hacktacular\IO\Database\Model\User;

abstract class UserController
{
	/**
	 * Attempts to Login the user
	 */
	public static function login(): void
	{
		// Shorthand notation
		$query = Request::$query;

		// Retrieve required values
		$user   = (string)$query->get('user');
		$passwd = (string)$query->get('passwd');

		// If the required data isn't set let the client know.
		if ($user === '' || $passwd === '') {
			ResponseFactory::respond('', BaseResponse::HTTP_BAD_REQUEST);
			return;
		}

		// If the login attempt was successful we will let the client know
		if (Login::performLogin($user, $passwd)) {
			$identity = IdentityFactory::getInstance();

			// the bootstrap makes our identity, so having it null here means something is up.
			if ($identity === null) {
				throw new \LogicException('Unexpected missing identity...');
			}

			// Response
			$token = array(
				'token' =>$identity->createToken()
			);

			ResponseFactory::respond(json_encode($token), BaseResponse::HTTP_OK);
			return;
		}

		// By default we will let the client know that they have failed.
		ResponseFactory::respond('', BaseResponse::HTTP_UNAUTHORIZED);
	}

	/**
	 * Responds current user's information
	 */
	public static function info(): void
	{
		$identity = IdentityFactory::getInstance();

		if ($identity === null || $identity->getUserId() === null) {
			// Not logged in, so no rights to "view" their user information
			ResponseFactory::respond('', BaseResponse::HTTP_UNAUTHORIZED);
			return;
		}

		$userId = (int)$identity->getUserId();

		// Retrieve and load user
		$user = User::find($userId);

		// Send response
		ResponseFactory::respond(json_encode($user), BaseResponse::HTTP_OK);
	}

	/**
	 * Logout will destroy the current Identity and Session
	 */
	public static function logout(): void
	{
		// Removes the identity
		IdentityFactory::removeInstance();

		// Removes the session
		session_destroy();

		// Reply with our success
		ResponseFactory::respond('', BaseResponse::HTTP_NO_RESPONSE);
	}

	/**
	 * Registers a user
	 */
	public static function register(): void
	{
		// Shorthand notation
		$query = Request::$query;

		// Retrieve required values
		$user   = (string)$query->get('user');
		$passwd = (string)$query->get('passwd');

		// If the required data isn't set let the client know.
		if ($user === '' || $passwd === '') {
			ResponseFactory::respond('', BaseResponse::HTTP_BAD_REQUEST);
			return;
		}

		$encsalt   = Encryption::createSalt();
		$encpasswd = Encryption::hash($passwd, $encsalt);

		$data = array(
			 'name'   => $user
			,'passwd' => $encpasswd
			,'salt'   => $encsalt
		);

		// Save the user data
		Query::save($data, array('users'));

		ResponseFactory::respond('', BaseResponse::HTTP_CREATED);
	}

	/**
	 * Retrieves identity, when not found replies 401.
	 * @return Identity
	 */
	private static function getIdentity(): ?Identity
	{

	}
}