<?hh // strict

namespace Hacktacular\IO\Websocket;

use Hacktacular\Core\Setting\SettingSource;

class Emitter
{
	/**
	 * The Redis connection used for emitting.
	 * @var \Redis
	 */
	private \Redis $redis;

	/**
	 * Redis key
	 */
	private string $key = '';

	/**
	 * Rooms
	 * @var vector
	 */
	private Vector<string> $rooms = Vector {};

	/**
	 * Flags
	 * @var vector
	 */
	private Map<string, string> $flags = Map {};

	/**
	 * Creates an instance of emitter
	 */
	public function __construct(): void
	{
		// Initialize the emitter
		$this->initialize();
	}

	/**
	 * Initializes the emitter
	 */
	private function initialize(): void
	{
		// Set key
		$this->key  = (string)SettingSource::getSetting('key', 'redis');

		$host = SettingSource::getSetting('host', 'redis');
		$port = SettingSource::getSetting('port', 'redis');

		$this->redis = new \Redis();
		$this->redis->connect($host, $port);
	}

	/**
	 * Adds a room to emit to.
	 */
	public function addRoom(string $room): void
	{
		$this->rooms->add($room);
	}

	/**
	 * Resets the rooms
	 */
	public function resetRooms(): void
	{
		$this->rooms = Vector {};
	}

	/**
	 * Adds a room to emit to.
	 */
	public function addFlag(string $flag, string $value): void
	{
		$pair = Pair { $flag, $value };

		$this->flags->add($pair);
	}

	/**
	 * Emits supplied messages and events through added rooms.
	 * If no room was added the message will be pushed on the public channel.
	 */
	public function emit(): this
	{
		// Get arguments
		$args = func_get_args();

		$packet = array();

		// Set defaults
		$packet['nsp'] = '/';
		$packet['data'] = $args;

		// set namespace
		if ($this->flags->contains('nsp')) {
			$packet['nsp'] = $this->flags->get('nsp');
		}

		// Pack the message
		$packed = \msgpack_pack(array($packet, array(
			 'rooms' => $this->rooms
			,'flags' => $this->flags
		)));

		// Publish message
		$this->redis->publish($this->key, $packed);

		return $this;
	}	
}