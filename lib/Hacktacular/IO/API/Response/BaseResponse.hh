<?hh //strict

namespace Hacktacular\IO\API\Response;

abstract class BaseResponse
{
	/**
	 * Accept code constants
	 */
	const int HTTP_OK          = 200;
	const int HTTP_CREATED     = 201;
	const int HTTP_ACCEPTED    = 202;
	const int HTTP_PARTIAL     = 203;
	const int HTTP_NO_RESPONSE = 204;

	/**
	 * Redirect code constants
	 */
	const int HTTP_MOVED        = 301;
	const int HTTP_FOUND        = 302;
	const int HTTP_METHOD       = 303;
	const int HTTP_NOT_MODIFIED = 304;

	/**
	 * Bad code constants
	 */
	const int HTTP_BAD_REQUEST  = 400;
	const int HTTP_UNAUTHORIZED = 401;
	const int HTTP_PAYMENT_REQ  = 402;
	const int HTTP_FORBIDDEN    = 403;
	const int HTTP_NOT_FOUND    = 404;

	/**
	 * Server problem code constants
	 */
	const int HTTP_INTERNAL_ERROR  = 500;
	const int HTTP_NOT_IMPLEMENTED = 501;
	const int HTTP_CONGESTED       = 502;
	const int HTTP_TIMEOUT         = 503;

	/**
	 * Creates a new instance of Response
	 */
	public function __construct(): void
	{
		
	}

	/**
	 * Registers the response handler
	 * @return bool Whether the registration was successful
	 */
	abstract protected function register(): bool;

	/**
	 * Modifies the header and sents it
	 * @return bool Whether or not modifying headers succeeded
	 */
	abstract public function header(): bool;

	/**
	 * Verify the content
	 * @param string $content The content
	 * @return bool Whether or not the content is valid to output
	 */
	abstract protected function verify(string $content): bool;

	/**
	 * Creates and outputs a response.
	 * @param string $content The content to output (in this case should be proper JSON)
	 * @return bool Whether or not the output was successful
	 */
	public function respond(string $content, int $code = self::HTTP_OK): bool
	{ 
		// Call headers
		$this->header();

		// Validate JSON if incorrect, inform the client and the application
		if (!$this->verify($content)) {
			http_response_code(self::HTTP_INTERNAL_ERROR);
			return false;
		}

		// Output
		http_response_code($code);
		echo $content;

		// Report our glorious victory
		return true;
	}
}