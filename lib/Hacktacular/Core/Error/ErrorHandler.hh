<?hh // strict

namespace Hacktacular\Core\Error;

use Hacktacular\Core\Setting\SettingSource;

abstract class ErrorHandler
{
	/**
	 * Adds an error to the indicated log.
	 * @param \Exception
	 */
	public static function addError(\Exception $ex): void
	{
		
	}

	/**
	 * Registers the ErrorHandler 
	 */
	public static function register(): void
	{
		// Register the ErrorHandler
		set_error_handler((__CLASS__ . '::error'));
		set_exception_handler((__CLASS__ . '::exception'));
	}

	/**
	 * ErrorHandling
	 */
	public static function error(): void
	{
		$error = error_get_last();

		// No errors ? no error handling!
		if ($error === null) {
			return;
		}
	}

	/**
	 * ExceptionHandling
	 * @param \Exception
	 */
	public static function exception(\Exception $exception): void
	{
		// UNSAFE
		$debug      = (int)SettingSource::getSetting('debug', 'debug');
		$err_format = (string)SettingSource::getSetting('err_format', 'debug');

		// Compile error message
		$err_message = sprintf($err_format,
			 $exception->getFile()
			,$exception->getLine()
			,$exception->getMessage()
			,$exception->getTraceAsString()
		);

		if ($debug) {
			// Replace the \n for logging with PHP_EOL's.
			$output_message = str_replace('\n', PHP_EOL, $err_message);
			echo '<pre>' . htmlentities($output_message) . '</pre>';
		}

		ErrorLog::logError('');
	}
}