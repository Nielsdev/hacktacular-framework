<?hh // strict

namespace Hacktacular\IO\Database;

use Hacktacular\IO\Database\Delegate\Delegate;

abstract class Query
{
	/**
	 * Executes a Query
	 * @param string $statement either a Query or a statement to execute
	 * @param array $data
	 * @return PDOStatement|null Returns PDOStatement or null on failure.
	 */
	public static function exec(mixed $statement, ?array<string, array<int, mixed>> $values = null, ?\PDO $link = null): ResultSource
	{
		if (!($statement instanceof ResultSource)) {
			// Prepare a statement for later execution
			$statement = self::prepare($statement);
		}

		if ($values !== null) {
			// Bind parameters supplied in the values array.
			self::bindParams($statement, $values);
		}

		try {
			$statement->execute();
		}
		catch (\PDOException $ex) {
			// @todo implement error handling
		}
		catch (\Exception $ex) {
			// @todo implement error handling
		}

		return $statement;
	}

	/**
	 * Prepares a Query
	 * @return ResultSource
	 */
	public static function prepare(string $query)
	{
		if ($link === null) {
			$link = LinkFactory::getLink();
		}

		return $link->prepare($query);
	}

	/**
	 * Retrieves the load statement for supplied object.
	 * @param DbObject or parent thereof
	 * @return \PDOStatment or parent thereof
	 */
	public static function getLoadStatement(DbObject $object): \PDOStatement
	{
		$tables = $object->getTables();
		$table  = reset($tables);

		$query = sprintf('SELECT * FROM "%s" WHERE "%s" = :id', $table, DbObject::ID_FIELD);

		$statement = self::prepareQuery($query);

		return $statement;
	}

	/**
	 * Saves given array to given table
	 */
	public static function save(Map<string, mixed> $data, string $table): void
	{
		if (array_key_exists(Delegate::ID_FIELD, $data) && (int)$data[Delegate::ID_FIELD] !== 0) {
			$query = 'UPDATE "%s" SET (%s) = (%s) WHERE "%s" = :%4$s';
		}
		else {
			$query = 'INSERT INTO "%s" (%s) VALUES (%s)';

			// UNSAFE
			// Unset the ID, because we are inserting
			unset($data[Delegate::ID_FIELD]);
		}

		$fields = array_keys($data);

		// Compile insert/update query
		// @todo this could use some readability
		$query = sprintf($query,
			 $table
			,'"'.implode('","', $fields) . '"'
			,':'.implode(', :', $fields)
			,Delegate::ID_FIELD
		);

		// Prepare the statement
		$statement = self::prepareQuery($query);

		// Bind the values
		self::bindValues($statement, $data);

		// Execute statement
		$statement->execute();
	}

	/**
	 * Returns the PDO Parameter Type of supplied variable
	 * @param mixed $var
	 * @return integer PDO_PARAM
	 */
	public static function getParamTypeOfVar(mixed $var): int
	{
		switch (gettype($var)) {
			case 'boolean':
				return \PDO::PARAM_BOOL;
			case 'integer':
				return \PDO::PARAM_INT;
			case 'double':
			case 'string':
			default:
				return \PDO::PARAM_STR;
		}
	}

	/**
	 * Prepares a query and returns the statement
	 * @param string
	 * @return \PDOStatement
	 */
	private static function prepareQuery(string $query): \PDOStatement
	{
		$link = LinkFactory::getLink();
		$statement = $link->prepare($query);

		return $statement;
	}

	/**
	 * Bind values to statement
	 * @param \PDOStatement
	 * @param array
	 */
	private static function bindValues(\PDOStatement $statement, array<string, mixed> $data): void
	{
		// Bind all supplied data entries as Value
		foreach ($data as $fieldname => $fieldvalue) {
			// Prepare parameter
			$fieldname = ':'.$fieldname;

			$statement->bindValue($fieldname, $fieldvalue, self::getParamTypeOfVar($fieldvalue));
		}
	}

	/**
	 * Binds an array of parameters to a PDOStatement
	 * @param \PDOStatement
	 * @param array
	 */
	private static function bindParams(\PDOStatement $statement, array<string, array<int, mixed>> $values): void
	{
		foreach ($values as $param => $value) {
			$statement->bindParam($param, current($value), key($value));
		}
	}
}