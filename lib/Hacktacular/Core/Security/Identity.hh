<?hh // strict

namespace Hacktacular\Core\Security;

use Hacktacular\User\User;
use Hacktacular\Core\Cache\CacheController;

class Identity
{
	/**
	 * Token length
	 * @var integer
	 */
	const int TOKEN_LENGTH = 64;

	/**
	 * Max keys, The amount of keys per type that can be in use for one identity
	 * @var integer
	 */
	const int MAX_KEYS = 3;

	/**
	 * Maximum time an Identity is saved for.
	 * @var integer
	 */
	const int IDENTITY_EXPIRE = 1500;

	/**
	 * Key type constants
	 * @var integer
	 */
	const int TYPE_SESSION    = 1;
	const int TYPE_TOKEN      = 2;

	/**
	 * The map containing authentication tokens
	 * @var map
	 */
	private Map<int, Vector<string>> $keys = Map {};

	/**
	 * Members that define this identity
	 */
	private ?int $userId = null;

	/**
	 * Initializes the Identity and starts the session
	 */
	public function __construct(): void
	{

	}

	/**
	 * Attempts to regenerate a session
	 */
	public function regenerateSession(): bool
	{
		// Get the section with session-keys
		$keys = $this->keys->get(self::TYPE_SESSION);

		if ($keys === null) {
			return false;
		}

		// Always ressurect the "latest" session
		$key = $keys->get(0);

		// Intialize the identity and start the session
		if ($key === null) {
			return false;
		}

		// Set session details
		session_id($key);
		session_start();

		return (session_id() == $key);
	}

	/**
	 * Retrieves the userId
	 * @return integer
	 */
	public function getUserId(): ?int
	{
		return $this->userId;
	}

	/**
	 * Set the UserId for this identity
	 * @param int $userId
	 */
	public function setUserId(int $userId): void
	{
		$this->userId = $userId;

		// Bind user to identity immediatly
		$this->save();
	}

	/**
	 * Adds a SessionId
	 * @param string
	 */
	public function addSessionId(string $sessionid): void
	{
		$this->storeKey($sessionid, self::TYPE_SESSION);
	}

	/**
	 * Creates a token
	 * @return string
	 */
	public function createToken(): string
	{
		$token = $this->createKey(self::TYPE_TOKEN);

		// Bind token to session immediatly
		$this->save();

		return $token;
	}

	/**
	 * Binds a token to this Identity
	 */
	public function bindToken(string $key): void
	{
		$this->storeKey($key, self::TYPE_TOKEN);
	}

	/**
	 * Creates a key of supplied type
	 * @param int $type
	 * @return string $key
	 */
	private function createKey(int $type): string
	{
		$key = Encryption::createSalt(self::TOKEN_LENGTH);

		if (!$this->storeKey($key, $type)) {
			throw new \OutOfBoundsException('Too many tokens of type :='.$type);
		}

		return $key;
	}

	/**
	 * Stores a key in the collection
	 * @param string $key
	 * @param integer $type
	 * @return boolean Whether or not the key was successfully inserted
	 */
	private function storeKey(string $key, int $type): bool
	{
		// Add vector if not yet created
		if (!$this->keys->contains($type)) {
			$pair = Pair { $type, Vector {} };
			$this->keys->add($pair);
		}

		$vector = $this->keys->get($type);

		if ($vector === null || count($vector) >= self::MAX_KEYS) {
			return false;
		}

		$vector->add($key);

		return true;
	}

	/**
	 * Deletes the current identity
	 */
	public function delete(): void
	{
		foreach ($this->keys as $type => $keys) {
			foreach ($keys as $key) {
				CacheController::delete('identity', array($key));
			}
		}
	}

	/**
	 * Saves this Identity
	 */
	public function save(): void
	{
		foreach ($this->keys as $type => $keys) {
			foreach ($keys as $key) {
				CacheController::add('identity', $this, array($key), self::IDENTITY_EXPIRE);
			}
		}
	}
}