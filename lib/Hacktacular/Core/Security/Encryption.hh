<?hh //strict

namespace Hacktacular\Core\Security;

abstract class Encryption
{
	/**
	 * Hashes supplied string
	 * @param string $value The value to be hashed
	 * @param string $salt (optional) salt to be added
	 * @param string $algorythm (optional) The algorythm to use, sha256 by default
	 * @return string The hashed string
	 */
	public static function hash(string $value, ?string $salt = '', ?string $algorythm = 'sha256'): string
	{
		return hash($algorythm, ($value . $salt));
	}

	/**
	 * Creates a salt and returns it
	 * @param integer $length The length of the Salt (24 by default)
	 * @return string The hexidecimal salt
	 */
	public static function createSalt(int $length = 24): string
	{
		$bytes = openssl_random_pseudo_bytes(($length / 2));
		$hex = bin2hex($bytes);

		return $hex;
	}
}