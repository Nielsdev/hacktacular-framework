<?hh // strict

namespace Hacktacular\Core\Cache;

use Hacktacular\Core\Setting\SettingSource;

abstract class CacheController
{
	/**
	 * Memcache instance
	 * @var \Memcached
	 */
	private static \Memcached $cache;

	/**
	 * Checks whether or not the servers have been added
	 * @var bool
	 */
	private static bool $loaded = false;

	/**
	 * Adds a value to Memcached
	 * @param string $key The key to cache under
	 * @param mixed $value The value to be cached
	 */
	public static function add(string $key, mixed $value, ?array<int, string> $dependencies = null, int $ttl = 3600): void
	{
		// Register the memcache servers if not yet registered.
		if (!self::$loaded) {
			self::register();
		}

		// Preprocess the value
		$value = self::compressValue($value);

		// Update cache key with dependencies
		$key = self::generateCacheKey($key, $dependencies);

		// Add the value to cache
		self::$cache->set($key, $value, $ttl);
	}

	/**
	 * Retrieves an item from Memcached
	 * @param string $key The key to retrieve with
	 * @return mixed
	 */
	public static function retrieve(string $key, ?array<int, string> $dependencies = null): mixed
	{
		// Register the memcache servers if not yet registered.
		if (!self::$loaded) {
			self::register();
		}

		// Update cache key with dependencies
		$key = self::generateCacheKey($key, $dependencies);

		// Decompresses the value
		$value = self::decompressValue(self::$cache->get($key));

		// Return the value retrieved
		return $value;
	}

	/**
	 * Deletes an item from Memcached
	 * @param string $key The key to delete with
	 */
	public static function delete(string $key, ?array<int, string> $dependencies = null): void
	{
		// Register the memcache servers if not yet registered.
		if (!self::$loaded) {
			self::register();
		}

		// Update cache key with dependencies
		$key = self::generateCacheKey($key, $dependencies);

		// Remove from cache
		self::$cache->delete($key);
	}

	/**
	 * Compresses the value, and prepares it for cache storage
	 * @param string $key The key to cache under
	 * @param array $depdencies Containg the dependencies
	 * @return string
	 */
	private static function generateCacheKey(string $key, ?array<int, string> $dependencies = null): string
	{
		$key .= ';' . implode(';', $dependencies);

		return $key;
	}

	/**
	 * Compresses the value, and prepares it for cache storage
	 * @return mixed
	 */
	private static function compressValue(mixed $value): mixed
	{
		// Find out if we need to process
		switch (gettype($value)) {
			case 'object':
			case 'array':
				return serialize($value);
			default:
				return $value;
		}
	}

	/**
	 * Decrompresses the value
	 * @return mixed
	 */
	private static function decompressValue(mixed $value): mixed
	{
		// Find out if we need to process
		$unserialized = @unserialize($value);

		return ($unserialized === false ? $value : $unserialized);
	}

	/**
	 * Registers the Cache Instance
	 */
	private static function register(): void
	{
		self::$cache = new \Memcached();

		// Retrieve memcached settings
		$host = SettingSource::getSetting('host', 'memcache');
		$port = SettingSource::getSetting('port', 'memcache');

		// Register the Server
		self::$cache->addServer($host, $port);

		// Set loaded true
		self::$loaded = true;
	}
}