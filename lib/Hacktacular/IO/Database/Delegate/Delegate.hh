<?hh // strict

namespace Hacktacular\IO\Database\Delegate;

use Hacktacular\IO\Database\Query;

abstract class Delegate
{
	const string ID_FIELD = 'id';

	/**
	 * Attempts to find a record
	 * @param mixed $value The value to search for
	 * @param string $column The column to search in
	 * @return Result
	 */
	public static function find(mixed $value, string $column = self::ID_FIELD): Result
	{
		$relation = self::composeRelationName();

		// Prepare load query and values.
		$query = sprintf('SELECT * FROM "%s" WHERE "%s" = :value', $relation, $column);
		$values = array(
			'value' => array(Query::getParamTypeOfVar($value) => $value)
		);

		// Execute & Retrieve statement
		$statement = Query::prepare($query);
		$statement->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Hacktacular\IO\Database\Delegate\Result');

		// Execute
		$statement = Query::exec($statement, $values);

		if (!count($statement)) {
			// Throws a great error
			throw new DelegateException('Entry with not found...');
		}

		// Fetch the result
		$result = $statement->fetch();

		// Inform Result of it's origin
		$result->setRelation($relation);

		return $result;
	}

	/**
	 * Creates a ResultSource based on the extending class
	 * @return Result
	 */
	public static function create(): Result
	{
		$relation = self::composeRelationName();

		$result = new Result();
		$result->setRelation($relation);

		return $result;
	}

	/**
	 * Returns the "default" table name
	 * @return string
	 */
	private static function composeRelationName(): string
	{
		$reflection = new \ReflectionClass(get_called_class());

		return strtolower($reflection->getShortName()).'s';
	}
}