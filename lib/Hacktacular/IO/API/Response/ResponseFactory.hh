<?hh // strict

namespace Hacktacular\IO\API\Response;

abstract class ResponseFactory
{
	/**
	 * Response types currently supported
	 */
	const int RESPONSE_JSON = 1;
	const int RESPONSE_XML  = 2;

	/**
	 * Creates a Response object by supplied Type
	 * @param integer $type
	 * @return BaseResponse An instantiated version
	 */
	public static function createResponse(int $type): BaseResponse
	{
		switch ($type) {
			case self::RESPONSE_JSON:
				return new JsonResponse();
		}
	}

	/**
	 * Writes a response to the output buffer
	 * @param string $content The content
	 * @param integer $type The output type
	 * @return bool Whether or not the output was successful
	 */
	public static function respond(string $content, int $code = BaseResponse::HTTP_OK, int $type = self::RESPONSE_JSON): bool
	{
		$response = self::createResponse($type);

		// Respond.
		return $response->respond($content, $code);
	}
}