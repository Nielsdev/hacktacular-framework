<?hh // strict

namespace Hacktacular\IO\Database;

/**
 * Wrapping class for the PDOStatement
 * Provides useful common functions executed by users
 */
class ResultSource extends \PDOStatement implements \JsonSerializable, \Countable
{
	/**
	 * Fetches records as array
	 * @return array
	 */
	public function toArray(): array<int, array<string, mixed>>
	{
		if (!$this->rowCount()) {
			return array();
		}

		return $this->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Retrieves a specific value from supplied field
	 * @param string $field
	 * @return mixed The value
	 */
	public function get(string $field): mixed
	{
		$result = $this->fetch(\PDO::FETCH_ASSOC);

		if (array_key_exists($field, $result)) {
			return $result[$field];
		}

		return false;
	}

	/**
	 * Countable implementation
	 * @return integer
	 */
	public function count()
	{
		return $this->rowCount();
	}

	/**
	 * Serializes the data
	 * @return array
	 */
	public function jsonSerialize(): array<int, array<string, mixed>>
	{
		return $this->toArray();
	}
}