<?hh // strict

namespace Hacktacular\User\Authentication\oAuth;

use Hacktacular\Core\Setting\SettingSource;
use Hacktacular\Core\Security\Encryption;

class Request
{
	/**
	 * End-point URL
	 * @var string
	 */
	private Map<string, string> $params = Map { };

	/**
	 * Authorization endpoint url
	 * @var string
	 */
	private string $authorization_endpoint = '';

	/**
	 * Creates a new instance of oAuth\Request
	 */
	public function __construct(string $auth_type): void
	{
		$this->initialize($auth_type);
	}

	/**
	 * Initializes data
	 * @param string $auth_type The authentication type (@see settings.ini)
	 */
	private function initialize(string $auth_type): void
	{
		// Retrieve the endpoint URL
		$this->authorization_endpoint = (string)SettingSource::getSetting($auth_type, 'oauth', 'authorization_endpoint');

		// Retrieve parameters
		$parameters = SettingSource::getSetting($auth_type, 'oauth', 'params');

		// If it's not an array, we have no use for it
		if (!is_array($parameters)) {
			// @todo error handling
			return;
		}

		// Create pairs
		foreach ($parameters as $key => $val) {
			$pair = Pair { $key, $val };
			$this->params->add($pair);
		}

		// Add state token
		$token = Pair {'state', self::createStateToken()};
		$this->params->add($token);
	}

	/**
	 * Returns state token
	 * @return string
	 */
	public function getStateToken(): ?string
	{
		return $this->params->get('state');
	}

	/**
	 * Creates and returns the Authentication URL
	 * @return string
	 */
	public function createAuthUrl(): string
	{
		// Compile URL
		$url = $this->authorization_endpoint . '?' . http_build_query($this->params);

		return $url;
	}

	/**
	 * Creates a Secret oAuth state token
	 * @return string
	 */
	private static function createStateToken(): string
	{
		return Encryption::createSalt(32);
	}
}