<?hh // strict

namespace Hacktacular\IO\Database;

use Hacktacular\Core\Setting\SettingSource;

abstract class LinkFactory
{
	/**
	 * Private static containing the PDO link
	 * @var \PDO
	 */
	static private ?\PDO $link = null;

	/**
	 * Retrieves the Link
	 * @return PDO PDO object
	 */
	public static function getLink(): \PDO
	{
		if (!(self::$link instanceof \PDO)) {
			self::$link = self::createLink();
		}

		return self::$link;
	}

	/**
	 * Creates the link with the database
	 * @return null|\PDO The PDO object or null upon failure.
	 */
	private static function createLink(): ?\PDO
	{
		// Retrieves the Settings for the database
		$dsn = SettingSource::getSetting('dsn', 'database');
		$usr = SettingSource::getSetting('user', 'database');
		$pwd = SettingSource::getSetting('passwd', 'database');

		$link = null;
		try {
			$link = new \PDO($dsn, $usr, $pwd);
			$link->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
			$link->setAttribute(\PDO::ATTR_STATEMENT_CLASS, array('Hacktacular\IO\Database\ResultSource', array($link)));
		}
		catch (\Exception $ex) {
			// We catch it here and then create a new exception.
			// We don't want the DSN (including the username and password to leak)
			throw new \PDOException('Could not connect to the database');
		}

		return $link;
	}
}