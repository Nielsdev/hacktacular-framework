<?hh // strict

namespace Hacktacular\User\Authentication;

use Hacktacular\IO\Database\Query;
use Hacktacular\Core\Security\Encryption;
use Hacktacular\Core\Security\IdentityFactory;

abstract class Login
{
	/**
	 * Performs a login
	 * @param string $user
	 * @param string $passwd
	 * @return bool Whether or not the login was successful
	 */
	public static function performLogin(string $user, string $passwd): bool
	{
		$values = array(
			'user' => array(\PDO::PARAM_STR => $user)
		);

		// Retrieve salt from database
		$result = Query::exec('SELECT salt FROM users WHERE name = :user', $values);
		$salt   = (string)$result->get('salt');

		// Combine salt with password and hash
		$passwd = Encryption::hash($passwd, $salt);

		// Check if username and password are correct
		$values['passwd'] = array(\PDO::PARAM_STR => $passwd);
		$result = Query::exec('SELECT id FROM users WHERE name = :user AND passwd = :passwd', $values);

		// Return result count
		if ($result->rowCount() <= 0) {
			return false;
		}

		// Retrieve userId
		$id = (int)$result->get('id');

		// Retrieve Identity and set user
		$identity = IdentityFactory::getInstance();

		// No identity ? no login!
		if ($identity === null) {
			return false;
		}

		// Set identity
		$identity->setUserId($id);

		return true;
	}
}