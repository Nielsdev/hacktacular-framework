<?hh // strict

namespace Hacktacular\Core\Routing;

abstract class Request
{
	/**
	 * ImmMap containing all URL data
	 */
	public static ImmMap<string, string> $url  = ImmMap {};

	/**
	 * ImmMap containing all Request data (_GET & _POST)
	 */
	public static ImmMap<string, mixed> $query = ImmMap {};

	/**
	 * ImmMap containing all Header (HTTP_) data (_SERVER)
	 */
	public static ImmMap<string, mixed> $headers = ImmMap {};

	/**
	 * Seperate initialize method to keep the constructor squeaky (and allow reinitialization)
	 * @param string $url
	 */
	public static function initialize(string $url): void
	{
		// Parse all required components
		self::parseUrl($url);
		self::parseQuery();
		self::parseHeaders();
	}

	/**
	 * Parses the URL and returns an ImmutableMap
	 * @param string
	 */
	private static function parseUrl(string $url): void
	{
		// Parses the URL and sanitizes the result
		$data = parse_url($url);

		// Create a map instead of an array
		$map = Map {};
		foreach ($data as $k => $v) {
			$pair = Pair { $k, $v };
			$map->add($pair);
		}

		// Make the Url data immutable
		self::$url = $map->toImmMap();
	}

	/**
	 * Retrieves the Globals for parsing
	 * @return array
	 */
	private static function retrieveGlobals():array<string, mixed>
	{
		// UNSAFE
		// Merge both, give presidency to POST
		$data   = array_merge($_GET, $_POST);

		// Also get the Angular way
		$json   = file_get_contents("php://input");
		$data   = array_merge($data, (array)json_decode($json));

		// Safety first; Destroy globals
		// We don't use unset because strict mode wants type safety.
		$_GET = $_POST = $_REQUEST = null;

		return $data;
	}

	/**
	 * Parses the Headers (SERVER) Sanitizes them and stores them
	 */
	private static function parseHeaders(): void
	{
		$map = Map{};
		// UNSAFE
		foreach ($_SERVER as $k => $v) {
			if (strpos($k, 'HTTP_') !== 0) {
				continue;
			}

			$k = substr($k, 5);
			$k = filter_var($k, FILTER_SANITIZE_STRING);
			$v = filter_var($v, FILTER_SANITIZE_STRING);

			$pair = Pair { $k , $v };

			$map->add($pair);
		}

		// Make the Header data immutable
		self::$headers = $map->toImmMap();
	}

	/**
	 * Parses the Query (POST/GET/GLOBALS) Sanitizes them and stores them
	 */
	private static function parseQuery(): void
	{
		// Retrieve globals
		$data = self::retrieveGlobals();

		$map = Map {};
		foreach ($data as $k => $v) {
			// We do this in the loop, because we want to sanitize the keys too.
			$k = filter_var($k, FILTER_SANITIZE_STRING);
			$v = filter_var($v, FILTER_SANITIZE_STRING);

			$pair = Pair { $k, $v };
			$map->add($pair);
		}

		// Make the Query data immutable
		self::$query = $map->toImmMap();
	}
}