<?hh // strict

namespace Hacktacular\Core\Setting;

abstract class SettingSource
{
	/**
	 * Path to Settings file (from location of the bootstrap.hh)
	 * @var string
	 */
	const string SETTING_SOURCE = 'config/settings.ini';

	/**
	 * Default settings section
	 * @var string
	 */
	const string DEFAULT_SECTION = 'misc';

	/**
	 * A boolean indicating whether or not the settings have been parsed
	 * @var Boolean
	 */
	private static bool $loaded = false;

	/**
	 * The Map containing the settings per section
	 * @var Map
	 */
	private static Map<string, Map<string, mixed>> $settings = Map {};

	/**
	 * Parses the configuration ini file
	 */
	private static function parseConfiguration(): void
	{
		// Retrieve the absolute path for the settings file
		$setting_file = self::getSettingsFile();

		// Parse the settings ini file to array
		$sections = parse_ini_file($setting_file, true);

		// Add the sections and settings
		foreach ($sections as $section => $settings) {
			$map = Map { };
			foreach ($settings as $setting => $value) {
				$pair = Pair { $setting, $value };
				$map->add($pair);
			}

			$section_pair = Pair { $section, $map };
			self::$settings->add($section_pair);			
		}
	}

	/**
	 * Retrieves the absolute path to the settings file.
	 * @return string
	 */
	private static function getSettingsFile(): string
	{
		$setting_file = \bootstrap\ROOT_DIR
			. DIRECTORY_SEPARATOR
			. self::SETTING_SOURCE
		;

		return $setting_file;
	}

	/**
	 * Retrieves a setting
	 * @param string $setting The setting you wish retrieve
	 * @param string $section The section you wish to retrieve it from
	 */
	public static function getSetting(string $setting, ?string $section = null, ?string $sub_section = null): mixed
	{
		// If the configuration hasn't been parsed before, do so!
		if (!self::$loaded) {
			self::parseConfiguration();
		}

		if ($section === null) {
			// Fallback to the default section if no section is supplied.
			$section = self::DEFAULT_SECTION;
		}

		// Attempts to retrieve the section
		$section = self::$settings->get($section);

		// We don't choose for a contains check here, because hh_client will have a hard time with it.
		if ($section === null) {
			throw new \InvalidArgumentException('Invalid section');
		}

		// See if the setting exists
		if (!$section->contains($setting)) {
			throw new \InvalidArgumentException('Invalid setting');
		}

		// Retrieve setting value
		$setting_value = $section->get($setting);
		if ($sub_section === null) {
			return $setting_value;
		}
		// See if the subsection is required and might exist
		if (!is_array($setting_value)) {
			throw new \InvalidArgumentException('Invalid sub-section');
		}

		return $setting_value[$sub_section];
	}
}