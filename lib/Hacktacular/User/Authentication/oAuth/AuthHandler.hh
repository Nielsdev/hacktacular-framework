<?hh // strict

namespace Hacktacular\User\Authentication\oAuth;

use Hacktacular\IO\API\Response\ResponseFactory;
use Hacktacular\Core\Cache\CacheController;

abstract class AuthHandler
{
	/**
	 * 
	 */
	public static function createAuthRequest(string $auth_type): void
	{
		// Set dependencies
		$dependencies = array($auth_type, session_id());
		$key = 'oauth';

		// Attempt to retrieve current oAuth request
		$request = CacheController::retrieve($key, $dependencies);

		// If not found, create and store it
		if (!($request instanceof Request)) {
			$request = new Request($auth_type);

			// Save request
			CacheController::add($key, $request, $dependencies);
		}

		$url = $request->createAuthUrl();

		$data = array(
			'url' => $url
		);

		// Parse response
		ResponseFactory::respond(json_encode($data, JSON_UNESCAPED_SLASHES));
	}

	/**
	 * Retrieves current state token
	 * @return string|null string the state token, or null if the request hasn't been made.
	 */
	public static function getCurrentStateToken(string $auth_type): ?string
	{
		$dependencies = array($auth_type, session_id());
		$key = 'oauth';

		// Attempt to retrieve current oAuth request
		$request = CacheController::retrieve($key, $dependencies);
		
		// If not found, create and store it
		if (!($request instanceof Request)) {
			return null;
		}

		return $request->getStateToken();
	}
}