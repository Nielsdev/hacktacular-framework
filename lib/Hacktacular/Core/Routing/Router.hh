<?hh // strict

namespace Hacktacular\Core\Routing;

abstract class Router
{
	/**
	 * Routing regular expressions
	 */
	const string MATCH_PATTERN   = '/{(.[a-z0-9]*)}/';
	const string REPLACE_PATTERN = '(.[a-z0-9]*)';
	const string ESCAPE_PATTERN  = '/\//';
	
	/**
	 * Map containg all Routes
	 */
	private static Map<string, mixed> $routes = Map {};

	/**
	 * Adds a route to the Router
	 * @param string $path The URL path to match on
	 * @param mixed $action The action to take on this path
	 */
	public static function addRoute(string $path, mixed $action): bool
	{
		$pair = Pair { $path, $action };
		self::$routes->add($pair);

		return self::$routes->contains($path);
	}

	/**
	 * Starts routing
	 */
	public static function route(): void
	{
		$path = Request::$url->get('path');

		// Determine action
		self::fireAction($path);
	}

	/**
	 * Matches the route(s) using regular expressions
	 */
	protected static function fireAction(?string $path): bool
	{
		// Shorthand
		$routes = self::$routes;

		// Regular expression matching
		foreach (self::$routes as $route => $action) {
			$pattern = self::compileRouteExpression($route);

			$matches = array();

			if (preg_match_all($pattern, $path, $matches)) {
				// Attempt to match the accoladed parameters
				//$match_names = array();
				//preg_match_all(self::MATCH_PATTERN, $route, $match_names);

				// Set-up the parameters
				// Remove the jitter created by the preg_match_all
				$match_params = array_slice($matches, 1, -1);

				// Due to the nature of preg_match_all we are still dealing with a multi-dimensional array
				// Let's loop and convert this to a neet little array
				$params = array();
				foreach ($match_params as $param) {
					$params[] = reset($param);
				}

				// Let's attempt a call
				call_user_func_array($action, $params);

				return true;
			}
		}

		// No action if we're still here
		return false;
	}

	/**
	 * Compiles the Route expression and returns it
	 * @param string The route path
	 * @return string The regular expression
	 */
	private static function compileRouteExpression(string $route): string
	{
		// create compatible string for routing
		$route = preg_replace(self::MATCH_PATTERN, self::REPLACE_PATTERN, $route);
		$route = preg_replace(self::ESCAPE_PATTERN, '\/', $route);
		$route = '/' . $route . '(\/?)$/';

		return $route;
	}
}