<?hh // strict

namespace Hacktacular\Core\Content;

use Hacktacular\IO\Database\DbObject;

class Content extends DbObject
{
	/**
	 * @var string
	 */
	protected string $tables = 'content';

	/**
	 * Content members
	 */
	private string $name = '';
	private string $description = '';
	private string $cid = '';

	/**
	 * Creates a new instance of Content
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Loads all data by array
	 * @param \PDOStatement or parent thereof
	 */
	protected function loadFromArray(array<string, mixed> $data): void
	{
		parent::loadFromArray($data);

		$this->name = (string)$data['name'];
		$this->description = (string)$data['description'];
		$this->cid = (string)$data['cid'];
	}

	/**
	 * Returns content-name
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * Returns description
	 */
	public function getDescription(): string
	{
		return $this->description;
	}

	/**
	 * Converts object members to array
	 * @return array
	 */
	public function toArray(): array<string, mixed>
	{
		return array_merge(parent::toArray(), array(
				 'name' => $this->name
				,'description' => $this->description
				,'cid' => $this->cid
		));
	}
}