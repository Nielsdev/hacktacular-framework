<?hh //strict

namespace Hacktacular\IO\API\Response;

class JsonResponse extends BaseResponse
{
	/**
	 * Creates a new instance of Response
	 */
	public function __construct(): void
	{
		parent::__construct();

		// Registers the response (currently broken.)
		// $this->register();
	}

	/**
	 * Modifies header where nescesary
	 */
	public function header(): bool
	{
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');

		return true;
	}

	/**
	 * Registers functions
	 */
	protected function register(): bool
	{
		// Register the header callback
		// Unfortunately the line below is currently broken.
		// Manual call is required in respond();
		//header_register_callback(array($this, 'header'));

		return true;
	}

	/**
	 * Verify the content
	 * @param string $content The content
	 * @return bool Whether or not the content is valid to output
	 */
	protected function verify(string $content): bool
	{
		json_decode($content);

		return (json_last_error() == JSON_ERROR_NONE);
	}
}