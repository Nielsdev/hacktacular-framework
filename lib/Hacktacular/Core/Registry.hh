<?hh // strict

namespace Hacktacular\Core;

abstract class Registry
{
	private static Map<string, mixed> $registry = Map {};
	
	public static function store(string $key, mixed $value): bool 
	{
		// Create a new pair for our Map
		$pair = Pair { $key, $value };

		// Add the pair to the Map
		self::$registry->add($pair);

		// Verify whether or not the collection contains the item.
		return self::$registry->contains($key);
	}

	public static function retrieve(string $key): mixed 
	{
		return self::$registry->get($key);
	}
}
