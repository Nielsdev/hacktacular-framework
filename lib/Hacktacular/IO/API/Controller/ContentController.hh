<?hh // strict

namespace Hacktacular\IO\API\Controller;

use Hacktacular\IO\Database\Query;
use Hacktacular\IO\API\Response\ResponseFactory;
use Hacktacular\IO\API\Response\BaseResponse;
use Hacktacular\Core\Content\Content;

abstract class ContentController
{
	/**
	 * Responds with the content item or 404
	 * @param string $cid
	 */
	public static function fetch(string $cid): void
	{
		$result = Query::find('content', 'cid', $cid);

		if (!$result->rowCount()) {
			ResponseFactory::respond('', BaseResponse::HTTP_NOT_FOUND);
			return;
		}

		$content = new Content();
		$content->loadFromStatement($result);

		ResponseFactory::respond(json_encode($content), BaseResponse::HTTP_OK);
	}
}