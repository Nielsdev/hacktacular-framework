<?hh // strict

namespace bootstrap;

require_once __DIR__ . '/lib/Psr4AutoloaderClass.hh';
require_once 'config/routing/routes.hh';
require_once 'config/routing/namespaces.hh';

use Hacktacular\Core\Routing\Request;
use Hacktacular\Core\Routing\Router;
use Hacktacular\Core\Error\ErrorHandler;
use Hacktacular\Core\Security\MemcacheSessionHandler;
use Hacktacular\Core\Security\IdentityFactory;

// Register directory
const string ROOT_DIR = __DIR__;

function main(): void
{
	// Create & Register PSR4 Auto-loader
	$loader = new \Psr4AutoloaderClass();
	$loader->register();

	// Register Hacktacular namespace
	$loader->addNamespace('Hacktacular', __DIR__ . '/lib/Hacktacular');

	// Register session handler
	$handler = new MemcacheSessionHandler();

	// Register Project namespace(s)
	addNamespaces($loader);

	// Register error handler
	ErrorHandler::register();

	// Parse URL
	// UNSAFE
	$url = '//'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

	// Initialize page request
	Request::initialize($url);

	// Retrieves instance of Identity, or creates it when empty
	IdentityFactory::startInstance();

	// Add routes from external file
	addRoutes();

	// Handle routing
	Router::route();
}

main();